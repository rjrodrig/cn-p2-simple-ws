#!/bin/bash

. /opt/cn_p2_simple_ws/venv/bin/activate
export DJANGO_SETTINGS_MODULE="cn_p2_simple_ws.settings"
for i in {1..60}; do
    sleep 2
    if echo "select 1;" | django-admin dbshell &> /dev/null; then
        break
    else
        echo "En espera de la Base de datos"
    fi
done
if [ ! -z $DO_MIGRATE ]; then
    exec django-admin migrate
else
    export GUNICORN_CMD_ARGS='--bind=0.0.0.0 --access-logfile="-"'
    exec gunicorn cn_p2_simple_ws.wsgi
fi
