from django.apps import AppConfig


class CnP2SimpleWs2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cn_p2_simple_ws'
